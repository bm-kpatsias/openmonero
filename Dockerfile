FROM ubuntu:18.04

RUN apt-get update \
  && apt-get install -y libmysql++-dev build-essential cmake libboost-all-dev miniupnpc libunbound-dev graphviz doxygen libunwind-dev pkg-config libssl-dev libcurl4-openssl-dev libgtest-dev libreadline-dev libzmq3-dev libsodium-dev libpcsclite-dev nano \
  && mkdir -p /opt/openmonero

WORKDIR /opt/openmonero

ADD . .

RUN mkdir /opt/openmonero/build

WORKDIR /opt/openmonero/build

RUN cmake .. && make
